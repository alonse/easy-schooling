<?php
session_start();
require('config.php');
if($_SESSION['login'] =='' || $_SESSION['login']!== 'admin'){
header("location:http://easy-schooling/reg.php");
};
if($_POST['exit'] == 'ok'){
  unset($_SESSION['login']);
  header("location:http://easy-schooling/reg.php");
};
?>
<?= require('header.php') ?>
<body class="theme-blue">
    <?php require('sidebar.php') ?>

    <section class="content">
        <div class="container-fluid">
            <!-- modal -->
            <?= require('content-admin.php'); ?>
            <!-- #END modal -->
        </div>
    </section>

<?= require ('footer.php'); ?>