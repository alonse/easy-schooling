<?php
session_start();
require('config.php');
if($_SESSION['login'] ==''){
header("location:http://easy-schooling/reg.php");
};

if($_POST['exit'] == 'ok'){
  unset($_SESSION['login']);
  header("location:http://easy-schooling/reg.php");
};

require ('header.php');
?>

    <?php require('sidebar.php') ?>
    <!-- END Sidebar -->

    <section class="content">
        <div class="container-fluid">
            <!-- Widgets -->
             <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                            <form action="" method="GET">
                            <div class="table-responsive">
                                    <table class="table table-bordered table-striped table-hover js-basic-example dataTable">
                                    <thead>
                                        <tr>
                                                <th><a href="index.php?sort=name">Учебник</a></th>
                                                <th><a href="index.php?sort=class">Класс</a></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <?php
                                                $id = $_GET['id'];
                                                $result=$pdo->query("SELECT * FROM `books`");
                                                while ($row=$result->fetch())
                                                {
                                                  echo '<tr><td>'.$row['name_book'].'</td><td>'.$row['class_book'].'</td><td><a name="go" href="book.php?id='.$row['id'].'">Открыть</a></td></tr>';
                                                }
                                            ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </form>
                            <div class="pagination">
                                <div class="dataTables_paginate paging_simple_numbers" id="DataTables_Table_0_paginate">
                                    <ul class="pagination">
                                        <li class="paginate_button active">
                                            <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="1" tabindex="0">1</a>
                                        </li>
                                        <li class="paginate_button">
                                            <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="2" tabindex="0">2</a>
                                        </li>
                                        <li class="paginate_button">
                                            <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="3" tabindex="0">3</a>
                                        </li>
                                        <li class="paginate_button next" id="DataTables_Table_0_next">
                                            <a href="#" aria-controls="DataTables_Table_0" data-dt-idx="4" tabindex="4">Next</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="book_text">

                </div>
            </div>
        </div>
    </div>

<?= require ('footer.php'); ?>