<?php
require ('../config.php');
$result=$pdo->query("SELECT * FROM `student`");
$row=$result->fetch();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Easy-Schooling</title>

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="../plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="../plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="../plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Morris Chart Css-->
    <link href="../plugins/morrisjs/morris.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="../css/style.css" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="../css/themes/all-themes.css" rel="stylesheet" />
</head>
<body class="theme-blue">
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="../index.php">E-BOOK</a>
        </div>
        <div class="navbar-right" style="padding: 20px;">
            <button type="button" data-toggle="modal" data-target="#myModal" class="btn btn-red">Добавить Учебник</button>
            <button type="button" data-toggle="modal" data-target="#myModal2" class="btn btn-red">Добавить Ученика</button>
            <button type="button" data-toggle="modal" data-target="#myModal3" class="btn btn-red">Добавить Учителя</button>
        </div>
    </div>
</nav>
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="../images/user.png" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <p><? echo $row['name']; ?> <? echo $row['surname']; ?></p>
                </div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="../profile.php"><i class="material-icons">person</i>Профиль</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">Навигация</li>
                <li class="active">
                    <a href="../index.php">
                        <i class="material-icons">home</i>
                        <span>Главная</span>
                    </a>
                    <a href="video.php">
                        <span>Видео</span>
                    </a>
                    <a href="audio.php">
                        <span>Аудио</span>
                    </a>
                    <a href="dop-mat.php">
                        <span>Доп. материалы</span>
                    </a>
                    <form action="http://book/admin.php">
                        <?php
                        if($_SESSION['login']== 'admin'){
                            echo '<button class="btn btn-primary admin_btn">Перейти в админ панель</button>';
                        }
                        ?>
                    </form>
                </li>

        </div>
        <!-- #Menu -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
<section class="content">
    <div class="container-fluid">

        <div class="row clearfix">
            video
        </div>
    </div>
</section>

<!-- Jquery Core Js -->

<script src="../plugins/jquery/jquery.min.js"></script>
<script src="../js/pages/forms/editor.js"></script>
<!-- Bootstrap Core Js -->
<script src="../plugins/bootstrap/js/bootstrap.js"></script>

<!-- Select Plugin Js -->
<script src="../plugins/bootstrap-select/js/bootstrap-select.js"></script>

<!-- Slimscroll Plugin Js -->
<script src="../plugins/jquery-slimscroll/jquery.slimscroll.js"></script>

<!-- Waves Effect Plugin Js -->
<script src="../plugins/node-waves/waves.js"></script>

<!-- Jquery CountTo Plugin Js -->
<script src="../plugins/jquery-countto/jquery.countTo.js"></script>

<!-- Morris Plugin Js -->
<script src="../plugins/raphael/raphael.min.js"></script>
<script src="../plugins/morrisjs/morris.js"></script>

<!-- Sparkline Chart Plugin Js -->
<script src="../plugins/jquery-sparkline/jquery.sparkline.js"></script>

<!-- editor -->
<script src="../plugins/ckeditor/ckeditor.js"></script>
<!-- Custom Js -->
<script src="../js/admin.js"></script>
<script src="../js/pages/index.js"></script>

<!-- Demo Js -->
<script src="../js/demo.js"></script>
</body>

</html>