<?php
require ('config.php');
$result=$pdo->query("SELECT * FROM `student`");
$row=$result->fetch();
?>
<section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="images/user.png" width="48" height="48" alt="User" />
                </div>
                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <p><? echo $row['name']; ?> <? echo $row['surname']; ?></p>
                    </div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="profile.php"><i class="material-icons">person</i>Профиль</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- Menu -->
            <div class="menu">
                <ul class="list">
                    <li class="header">Навигация</li>
                    <li class="active">
                        <a href="index.php">
                            <i class="material-icons">home</i>
                            <span>Главная</span>
                        </a>
                        <a href="pages/video.php">                           
                            <span>Видео</span>
                        </a>
                        <a href="pages/audio.php">    
                            <span>Аудио</span>
                        </a>
                        <a href="pages/dop-mat.php">
                            <span>Доп. материалы</span>
                        </a>
                        <form action="http://easy-schooling/admin.php">
                            <?php
                            if($_SESSION['login']== 'admin'){
                                echo '<button class="btn btn-primary admin_btn">Перейти в админ панель</button>';
                            }
                            ?>
                        </form>
                    </li>

            </div>
            <!-- #Menu -->  
        </aside>
        <!-- #END# Left Sidebar -->
    </section>
